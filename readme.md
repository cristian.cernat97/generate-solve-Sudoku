# Project Title

Generate and solve Sudoku

It generates a grid using backtracking and solves it also by using backtraking.

![Screenshot_33](/uploads/c5849d26a6832e8b62a660a4370fab2c/Screenshot_33.png)

### FlowChart
![FloawChartSudoku](/uploads/84088d9394607139feddec7145f20b1c/FloawChartSudoku.jpg)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.


### Prerequisites

What things you need to install the software and how to install them

```
https://www.visualstudio.com/downloads/
```

### Installing

A step by step series of examples that tell you have to get a development env running


```
 git clone https://gitlab.com/cristian.cernat97/generate-solve-Sudoku
```
## Built With

* [Backtracking](https://www.geeksforgeeks.org/backtracking-algorithms/) - Learning platform

## Contributing

Go ahead blame me for anything I am still learning 
## Versioning

Version 1.0 

## Authors

* **Cristian Cernat** - (https://gitlab.com/cristian.cernat97)
