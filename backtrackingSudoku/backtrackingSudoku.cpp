// backtrackingSudoku.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

//GLOBALS:
	// UNASSIGNED is used for empty cells in sudoku grid
	#define UNASSIGNED 0

	// N is used for size of Sudoku grid. Size will be NxN
	#define N 9

	//used for counting the attempts
	int tries = 0;


//FUNCTIONS:

	// This function finds an entry in grid that is still unassigned
	bool FindUnassignedLocation(int grid[N][N], int &row, int &col);

	// Checks whether it will be legal to assign num to the given row,col
	bool isSafe(int grid[N][N], int row, int col, int num);

	/* Takes a partially filled-in grid and attempts to assign values to
	all unassigned locations in such a way to meet the requirements
	for Sudoku solution (non-duplication across rows, columns, and boxes) */
	bool SolveSudoku(int grid[N][N]){
	
		int row, col;

		// If there is no unassigned location, we are done
		if (!FindUnassignedLocation(grid, row, col))
			return true; // success!

		// consider digits 1 to 9
		for (int num = 1; num <= 9; num++){

			// if it is safe to put
			if (isSafe(grid, row, col, num)){
				// make assignment
				grid[row][col] = num;

			// return, if success, yay!
			if (SolveSudoku(grid))
				return true;

			// failure, unmake & try again
			grid[row][col] = UNASSIGNED;
			}
		}
	return false; // this triggers backtracking
}

/* Searches the grid to find an entry that is still unassigned. If
found, the reference parameters row, col will be set the location
that is unassigned, and true is returned. If no unassigned entries
remain, false is returned. */
bool FindUnassignedLocation(int grid[N][N], int &row, int &col){
	for (row = 0; row < N; row++)
		for (col = 0; col < N; col++)
			if (grid[row][col] == UNASSIGNED)
				return true;
	return false;
}

/* Returns a boolean which indicates whether any assigned entry
in the specified row matches the given number else returns false. */
bool UsedInRow(int grid[N][N], int row, int num){
	for (int col = 0; col < N; col++)
		if (grid[row][col] == num)
			return true;
	return false;
}

/* Returns a boolean which indicates whether any assigned entry
in the specified column matches the given number else returns false. */
bool UsedInCol(int grid[N][N], int col, int num){
	for (int row = 0; row < N; row++)
		if (grid[row][col] == num)
			return true;
	return false;
}

/* Returns a boolean which indicates whether any assigned entry
within the specified 3x3 box matches the given number else returns false. */
bool UsedInBox(int grid[N][N], int boxStartRow, int boxStartCol, int num){
	for (int row = 0; row < floor(sqrt(N)); row++)
		for (int col = 0; col < floor(sqrt(N)); col++)
			if (grid[row + boxStartRow][col + boxStartCol] == num)
				return true;
	return false;
}

/* Returns a boolean which indicates whether it will be legal to assign
num to the given row,col location. */
bool isSafe(int grid[N][N], int row, int col, int num){
	/* Check if 'num' is not already placed in current row,
	current column and current 3x3 box */
	return !UsedInRow(grid, row, num) &&
		!UsedInCol(grid, col, num) &&
		// row - row % sqrt(N) gets the row of the current box
		// col - col % sqrt(N) gets the col of the current box
		!UsedInBox(grid, row - row % (int)(sqrt(N)), col - col % (int)(sqrt(N)), num);
}

/* A utility function to print grid  */
void printGrid(int grid[N][N]){
	for (int row = 0; row < N; row++) {
		for (int col = 0; col < N; col++) {
			//draws the separators for cols
			if (col % 3 == 0)printf(" | ");
			printf(" %2d ", grid[row][col]);
		}
		//draws the separators for rows
		if (row % 3 == 0)printf(" - ");
		printf("\n\n");
	}
	printf("\nEnd grid\n");
}

/* A function that generates random numbers using the 
row, col and the grid given. Checks if is safe a number to put in the grid.
*/
int gNumber(int grid[N][N], int row, int col) {
	int gNum;
	
	gNum = rand() % 10;
	if (isSafe(grid, row, col, gNum)) {
			return gNum;
	}
	else {
		//gNumber(grid, row, col, limit);//recursion in order to generate appropriate number
		return gNum = 0;
	}
}

/* A function that generates a grid for sudoku recursively until
 it can be solved and gives how many tries were made in order
 to reach a result.
*/
void gSudoku(int grid[N][N]) {
	//limit of numbers in grid
	int numLimit = 9;

	//get safe random numbers into the grid
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			grid[i][j] = gNumber(grid, i, j);
		}
	}
	//increment tries
	tries += 1;

	printf("\nGenerated:\n");
	printGrid(grid);

	//check if was solved 
	if (SolveSudoku(grid)) {
		printf("\nSolved:\n");
		printGrid(grid);
		printf("\nTried %d times", tries);
	}
	//if not it's generated again
	else {
		gSudoku(grid);
	}
}
/* MAIN call gSudoku, SEED and init grid */
int main(){
	//initialize seed ONLY ONE TIME
	srand((unsigned int)time(NULL));
	
	//initialize grids
	int grid[N][N] = {
		{ 0,0,0, 0,0,0, 0,0,0 },
		{ 0,0,0, 0,0,0, 0,0,0 },
		{ 0,0,0, 0,0,0, 0,0,0 },

		{ 0,0,0, 0,0,0, 0,0,0 },
		{ 0,0,0, 0,0,0, 0,0,0 },
		{ 0,0,0, 0,0,0, 0,0,0 },

		{ 0,0,0, 0,0,0, 0,0,0 },
		{ 0,0,0, 0,0,0, 0,0,0 },
		{ 0,0,0, 0,0,0, 0,0,0 }
	};

	//call generate sudoku
	gSudoku(grid);

	getchar();
	return 0;
}